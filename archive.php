<?php get_header(); 
// vars
            $queried_object = get_queried_object(); 
            $taxonomy = $queried_object->taxonomy;
            $term_id = $queried_object->term_id;
            $term_slug = $queried_object->slug;
             //echo $term_slug;
?>
			<div class="content main">
				<div class="col" id="main-content" role="main">

					<?php if (is_category()) { ?>
					<h1 class="archive-title">
						<?php single_cat_title(); ?>
					</h1>
					<?php } elseif (is_tag()) { ?>
					<h1 class="archive-title">
						<span><?php _e( 'Articles Tagged:', 'bonestheme' ); ?></span> <?php single_tag_title(); ?>
					</h1>
					<?php } elseif (is_author()) {
						global $post;
						$author_id = $post->post_author;
					?>
					<h1 class="archive-title">
						<span><?php _e( 'Articles By:', 'bonestheme' ); ?></span> <?php the_author_meta('display_name', $author_id); ?>
					</h1>
					<?php } elseif (is_day()) { ?>
					<h1 class="archive-title">
						<span><?php _e( 'Daily Archives:', 'bonestheme' ); ?></span> <?php the_time('l, F j, Y'); ?>
					</h1>
					<?php } elseif (is_month()) { ?>
					<h1 class="archive-title">
						<span><?php _e( 'Monthly Archives:', 'bonestheme' ); ?></span> <?php the_time('F Y'); ?>
					</h1>
					<?php } elseif (is_year()) { ?>
					<h1 class="archive-title">
						<span><?php _e( 'Yearly Archives:', 'bonestheme' ); ?></span> <?php the_time('Y'); ?>
					</h1>
					<?php } ?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">
						<h3 class="entry-title">
                            <?php if($term_slug !== 'newsletter'){ ?>
                                <a href="<?php the_permalink() ?>" rel="bookmark">                            
                                    <?php the_title(); ?>                                
                                </a>
                            <?php } else { ?>
                                <a href="<?php the_field('newsletter_file'); ?>" rel="bookmark">                            
                                    <?php the_title(); ?>                                
                                </a>
                            <?php } ?>
                        </h3>
						<section class="entry-content cf">
							<?php the_post_thumbnail( 'content-width' ); ?>
							<?php if (($term_slug == 'lecture-series') || ($term_slug == 'conferences')){
                                   
                                }  else {
                                        the_excerpt(); 
                                        }
                            ?>
                            <?php if($term_slug !== 'newsletter') { ?>
				                <a href="<?php the_permalink() ?>" class="btn">Read More</a>
                            <?php } ?>
                            <?php if(get_field('newsletter_file')) { ?>
                                <a href="<?php the_field('newsletter_file'); ?>" class="btn download">Download Newsletter</a>
                            <?php } ?>
						</section>
					</article>

					<?php endwhile; ?>
					
					<?php bones_page_navi(); ?>
					
					<?php else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but nothing is available at this address. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a>, <p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

					<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>
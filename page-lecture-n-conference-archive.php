<?php
/*
 Template Name: Lecture and Conference Archive Page
*/
    get_header(); 
    // vars
    $queried_object = get_queried_object(); 
    $taxonomy = $queried_object->taxonomy;
    $term_id = $queried_object->term_id;
    $term_slug = $queried_object->slug;
     //echo $term_slug;
?>
			<div class="content main archive">
				<div class="col" id="main-content" role="main">

					<h1><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
						</section>				
                    <?php $terms = get_field('category_selection');
                            $order_list = get_field('order_list');
                            if (!order_list){ $order_list = 'ASC';}
                    if( $terms ):  ?>
                                        
					<?php $core_loop = new WP_Query( array( 'category_name' => "'$terms->slug'", 'posts_per_page' => -1, 'orderby' => 'meta_value_num', 'meta_key' => 'start_date', 'order' => $order_list )); ?>
                    
                    <div class="<?php echo $terms->slug; ?>-list">
					<?php if ($core_loop->have_posts()) : while ($core_loop->have_posts()) : $core_loop->the_post(); ?>

                        
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">
						<h3 class="entry-title">                           
                            <?php the_title(); ?>
                        </h3>
                        <?php 
                            $start_date = get_field('start_date'); 
                            $end_date = get_field('end_date');
                        if($terms->slug !== 'conferences'):
                            if($start_date || $end_date):                        
                        ?>
                        <h5>
                            <?php 
                                echo $start_date;
                                if ($end_date):
                                    echo ' - '. $end_date;
                                endif;
                            ?>
                        </h5>
                        <?php endif; endif; ?>
						<section class="entry-content cf">
							<?php the_post_thumbnail( 'content-width' ); ?>
							<?php if (($terms->slug == 'lecture-series') || ($terms->slug == 'conferences')){
                                   
                                }  else {
                                        the_excerpt(); 
                                        }
                            ?>
				                <a href="<?php the_permalink() ?>" class="btn">Read More</a>
						</section>
                        </article>

					<?php endwhile; ?>
					
					<?php bones_page_navi(); ?>
					
					<?php else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but nothing is available at this address. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a>, <p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

					<?php endif; ?>
                    </div>
                    <?php endif; ?>
				    <?php wp_reset_postdata(); ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>